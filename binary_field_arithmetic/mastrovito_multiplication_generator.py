def function_matrix_p(m: int, f: list) -> dict:
    """
    Funcion que genera la matriz P necesaria para la multiplicacion de Matrovito

    :param m: tamano del campo
    :param f: lista con los coeficientes del polinomio irreducible
    :return: matriz P
    """
    matrix_p = dict()
    for i in range(m - 1):
        for j in range(m):
            matrix_p[f'P_{i}_{j}'] = 0

    for j in range(m):
        matrix_p[f'P_{0}_{j}'] = f[::-1][j]

    for i in range(1, m - 1):
        for j in range(m):
            if j == 0:
                matrix_p[f'P_{i}_{j}'] = matrix_p[f'P_{i - 1}_{m - 1}']
            else:
                matrix_p[f'P_{i}_{j}'] = matrix_p[f'P_{i - 1}_{j - 1}'] ^ (matrix_p[f'P_{i - 1}_{m - 1}'] & matrix_p[f'P_{0}_{j}'])

    # print("Resulado de la matriz P: ", '*' * 50)
    # for i in range(m - 1):
    #     for j in range(m):
    #         print(f"P_{i}_{j}' = {matrix_p[f'P_{i}_{j}']}")
    return matrix_p


def function_mastrovito_matrix(m: int, a: list, p: dict):
    """
    Funcion que genera la matriz de Matrovito con los coeficientes de a(x) en su forma general

    :param m: tamano del campo
    :param a: coeficientes de un elemento del campo GF(2^m)
    :param p: matriz P
    :return: matriz de Mastrovito
    """
    print(f'm={m}')
    print(f'a={a}')
    print(f'p={p}')
    matrix_z = dict()
    for i in range(m):
        for j in range(m):
            matrix_z[f'Z_{i}_{j}'] = ''
    for i in range(m):
        matrix_z[f'Z_{i}_{0}'] = a[i]
    for i in range(m):
        for j in range(1, m):
            for t in range(j):
                if p[f'P_{j - 1 -t}_{i}'] == 1:
                    matrix_z[f'Z_{i}_{j}'] = a[m - 1 - t] + '+' + matrix_z[f'Z_{i}_{j}']
            if i >= j:
                matrix_z[f'Z_{i}_{j}'] = a[i - j] + '+' + matrix_z[f'Z_{i}_{j}']
    print(matrix_z)
    return "Hello world"


if __name__ == '__main__':
    field_size = 6
    test_matrix_p = function_matrix_p(m=field_size, f=[1, 0, 1, 1, 0, 1, 1])
    # print(test_matrix_p)
    a_x = [f'a_{i}' for i in range(field_size)]
    test_matrix_z = function_mastrovito_matrix(m=field_size, a=a_x, p=test_matrix_p)


