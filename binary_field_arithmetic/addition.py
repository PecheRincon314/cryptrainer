from binary_field_arithmetic import hex_to_binary, binary_to_polynomial


def addition_mod_f(a: hex, b: hex) -> hex:
    """
    Funcion que implementa la suma en GF(2^m). Se requiere que ambos sumandos tengan la misma cantidad de digitos
    hexadecimales y que formen siempre m bits para evitar errores.

    :param a: polinomio elemento del campo con coeficientes en representacion hexadecimal
    :param b: polinomio elemento del campo con coeficientes en representacion hexadecimal
    :return: resultado de la suma modulo f(x)
    """
    return hex(a ^ b)


if __name__ == '__main__':
    m = 8  # addition GF(2^8)
    hex_a = 0xAB
    hex_b = 0xBC
    hex_f = 0x11D  # f(x) = x^8 + x^4 + x^3 + x^2 + 1 -- 100011101

    print(f'a = {binary_to_polynomial(list(hex_to_binary(hex_a, m)))}')
    print(f'b = {binary_to_polynomial(list(hex_to_binary(hex_b, m)))}')
    print(f'f = {binary_to_polynomial(list(hex_to_binary(hex_f, m + 1)))}')
    test = addition_mod_f(hex_a, hex_b)
    print(f'hex_addition = {test}')
    print(f'bin_addition = {hex_to_binary(int(test, 16), m)}')
    print(f'pol_addition = {binary_to_polynomial(list(hex_to_binary(int(test, 16), m)))}')
