def hex_to_binary(number: hex, nbits: int) -> str:
    """
    Funcion que representa en forma binaria un numero hexadecimal. El numero de bits con los que se quiere representar
    el numero hexadecimal tiene que ser mayor o igual al numero de bits que formarian el total de digitos hexadecimales.

    :param number: numero en formato hexadecimal
    :param nbits: numero de bits con los que se desea representar el hexadecimal en formato bianrio
    :return: cadena de unos y ceros
    """
    aux_format = '{0:0' + str(nbits) + 'b}'
    return aux_format.format(number)


if __name__ == '__main__':
    test = hex_to_binary(0x113, 12)
    print(test)
