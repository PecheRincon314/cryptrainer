def str_list_to_int_list(a: list) -> list:
    """
    Funcion que convierte una lista de unos y ceros en formato str a una lista de unos y ceros en formato int

    :param a: lista de ceros y unos en formato str
    :return: lista de ceros y unos en formato int
    """
    return [int(i) for i in a]


if __name__ == '__main__':
    test = str_list_to_int_list(['1', '0', '1', '0'])
    print(test)
