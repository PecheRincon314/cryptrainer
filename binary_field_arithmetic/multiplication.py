from numpy import log2, ceil, array
# ====================================================================================================
# Matrix-Vector Multipliers - Mastrovito product matrix
# ====================================================================================================

from binary_field_arithmetic import binary_to_polynomial, hex_to_binary, str_list_to_int_list
from binary_field_arithmetic.squaring import m2xvv, m2abv, product_alpha_a


def m2xor(x: int, y: int) -> int:
    """

    :param x: digito binario
    :param y: digito binario
    :return: x XOR Y
    """
    return x ^ y


def m2and(x: int, y: int) -> int:
    """

    :param x: digito binario
    :param y: digito binario
    :return: x AND y
    """
    return x & y


def matrix_p(f: hex) -> array:
    """
    Calcula la matriz P

    :param f: coeficientes del polinomio irreducible en representacion hexadecimal
    :return: matriz P
    """
    m = int(ceil(log2(f)))
    f = list(bin(f))[::-1]
    p = array([[0] * (m - 1)] * (m - 2))
    for j in range(m - 1):
        p[0][j] = f[j]

    for i in range(1, m - 2):
        for j in range(m - 1):
            if j == 0:
                p[i][j] = p[i - 1][m - 2]
            else:
                p[i][j] = m2xor(p[i - 1][j - 1], m2and(p[i - 1][m - 2], p[0][j]))
    return p


def mastrovito_matrix(a: hex, p: array) -> array:
    """
    Calcula la matriz Z de Mastrovito

    :param a: coeficientes del polinomio a(x) en representacion hexadecimal
    :param p: matriz P
    :return: matriz Z
    """
    m = len(p[0])
    z = array([[0] * m] * m)
    aux_format = '{0:0' + str(m) + 'b}'
    a = list(aux_format.format(a))[::-1]
    a = [int(i) for i in a]
    for i in range(m):  # for i in 0 .. m-1
        z[i][0] = a[i]
    for i in range(m):  # for i in 0 .. m-1
        for j in range(1, m):  # for j in 1 .. m-1 loop
            for t in range(j):  # for t in 0 .. j-1
                z[i][j] = m2xor(z[i][j], m2and(p[j - 1 - t][i], a[m - 1 - t]))
            if i >= j:
                z[i][j] = m2xor(a[i - j], z[i][j])
    return z


def mastrovito_multiplication(a: hex, b: hex, f: hex) -> list:
    """
    Calcula la multiplicación de Mastrovito

    :param a: coeficientes del polinomio a(x) en representacion hexadecimal
    :param b: coeficientes del polinomio b(x) en representacion hexadecimal
    :param f: coeficientes del polinomio irreducible en representacion hexadecimal
    :return: a(x) * b(x) mod f(x)
    """
    p = matrix_p(f)
    z = mastrovito_matrix(a, p)
    m = len(p[0])
    aux_format = '{0:0' + str(m) + 'b}'
    b = list(aux_format.format(b))[::-1]
    b = [int(i) for i in b]
    c = [0] * m
    for i in range(m):  # for i in 0 .. m-1
        for j in range(m):  # for j in 0 .. m-1
            c[i] = m2xor(c[i], m2and(z[i][j], b[j]))
    return c[::-1]


def lsb_first_multiplier(a: list, b: list, f: list) -> list:
    """
    Funcion que calcula la Multiplicación intercalada. Algorithm 7.3—LSB-first multiplier de la referencia:
    HardwareImplementation of Finite-Field Arithmetic - Jean-Pierre Deschamps, José Luis Imaña, Gustavo D. Sutter

    :param a: lista de los coeficientes del elemento a(x)
    :param b: lista de los coeficientes del elemento b(x)
    :param f: lista de los coeficientes del polinomio irreducible f(x)
    :return: c(x) = a(x) * b(x) mod f(x)
    """
    a = str_list_to_int_list(a)[::-1]  # [a0, a1,...,am-1]
    b = str_list_to_int_list(b)[::-1]  # [b0, b1,...,bm-1]
    f = str_list_to_int_list(f)[::-1]  # [f0, f1,...,fm-1, 1]
    m = len(f) - 1
    c = [0] * m
    for i in range(m):
        c = m2xvv(m2abv(b[i], a, m), c, m)
        a = product_alpha_a(a, f, m)
    return c  # [c0, c1,...,cm-1]


if __name__ == '__main__':
    # test_matrix_p = matrix_p(0x5b)
    # print(test_matrix_p)
    # test_mastrovito_matrix = mastrovito_matrix(0xb, test_matrix_p)
    # print(test_mastrovito_matrix)
    m = 8  # multiplication in GF(2^8)
    hex_a = 0xAB
    hex_b = 0xBC
    hex_f = 0x11D  # f(x) = x^8 + x^4 + x^3 + x^2 + 1 -- 100011101
    print(f'a = {binary_to_polynomial(list(hex_to_binary(hex_a, m)))}')
    print(f'b = {binary_to_polynomial(list(hex_to_binary(hex_b, m)))}')
    print(f'f = {binary_to_polynomial(list(hex_to_binary(hex_f, m + 1)))}')
    test_mastrovito_multiplication = mastrovito_multiplication(hex_a, hex_b, hex_f)
    # print(test_mastrovito_multiplication)
    # print(list(''.join(str(i) for i in test_mastrovito_multiplication)))
    print(f"pol_mastrovito_multiplication = {binary_to_polynomial(list(''.join(str(i) for i in test_mastrovito_multiplication)))}")

    test_lsb_first_multiplier = lsb_first_multiplier(list(hex_to_binary(hex_a, m)), list(hex_to_binary(hex_b, m)),
                                                     list(hex_to_binary(hex_f, m)))
    # print(test_lsb_first_multiplier)
    print(f"pol_lsb_first_multiplier      = {binary_to_polynomial(list(''.join(str(i) for i in test_lsb_first_multiplier[::-1])))}")
