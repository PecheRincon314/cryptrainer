from math import floor
from random import getrandbits

from prime_field_arithmetic import montgomery_multiplication
from prime_field_arithmetic.generating_random_prime import generate_prime
from prime_field_arithmetic.radix_b_representation import radix_b_representation


def multiply(numbers):
    total = 1
    for x in numbers:
        total *= x
    return total


def exponentiation(base: int, exponent: int, prime_number: int) -> int:
    """

    :param base: Base de la operacion
    :param exponent: Exponente de la operacion
    :param prime_number: Numero primo
    :return: Potencia modulo p
    """
    bin_exponent = "{0:b}".format(exponent)
    # print(f'binary = {bin_exponent}')
    x = list()
    counter = -1
    for i in bin_exponent[::-1]:
        counter += 1
        if i == '1':
            # print(f'i = {i} - count = {counter}')
            x.append((base ** (2 ** counter)) % prime_number)
    x = multiply(x)
    return x % prime_number


# ---------------------------------------------------------------------------------------------------------------------
# Techniques for general exponentiation
# ---------------------------------------------------------------------------------------------------------------------


def right_to_left_binary_exponentiation(g: int, e: int, m: int) -> int:
    """

    :param g: base
    :param e: exponent
    :param m: module
    :return: g ^ e mod m
    """
    a = 1
    s = g
    while e != 0:
        if e % 2 == 1:
            a = a * s
        e = floor(e / 2)
        if e != 0:
            s = s * s
    return a % m


def left_to_right_binary_exponentiation(g: int, e: int, m: int) -> int:
    """

    :param g: base
    :param e: exponent
    :param m: module
    :return: g ^ e  mod m
    """
    a = 1
    e_sub_i = radix_b_representation(e, 2)
    for i in range(len(e_sub_i)):
        a = a * a
        if e_sub_i[i] == 1:
            a = a * g
    return a % m


def addition_chaining_exponentiation(x: int, y: int, n: int) -> int:
    """

    :param x: base
    :param y: exponent
    :param n: module
    :return: x ** y mod n

    reference: pag. 346 APPLIED CRYPTOGRAPHY, SECOND EDITION: Protocols, Algorithms, and Source Code in C:Table of Contents

    """
    s = 1
    t = x
    u = y
    while u:
        if u & 1:
            s = (s * t) % n
        u >>= 1
        t = (t * t) % n
    return s


def fast_exp(x: int, y: int, n: int) -> int:
    """

        :param x: base
        :param y: exponent
        :param n: module
        :return: x ** y mod n

        reference: pag. 346 APPLIED CRYPTOGRAPHY, SECOND EDITION: Protocols, Algorithms, and Source Code in C:Table of Contents

        """
    if y == 1:
        return x % n
    if y & 1 == 0:
        tmp = fast_exp(x, int(y / 2), n)
        return (tmp * tmp) % n
    else:
        tmp = fast_exp(x, int((y - 1) / 2), n)
        tmp = (tmp * tmp) % n
        tmp = (tmp * x) % n
        return tmp


def montgomery_exponentiation(x: str, e: str, m: str, b: int) -> int:
    """

    :param x: exponentiation base m > x >= 1
    :param e: exponent e = (et ··· e0)2 with et = 1,
    :param m: moduli m = (ml−1 ··· m0)b,
    :param b: radix of the numbering system gcd(m, R)=1
    :return: x^e mod m
    """
    e_sub_i = radix_b_representation(int(e), 2)
    r_squared_mod_m = str(((b ** len(m)) ** 2) % int(m))
    x_mark = montgomery_multiplication(m, x, r_squared_mod_m, b)
    a = (b ** len(m)) % int(m)
    for e_i in e_sub_i:
        a = montgomery_multiplication(m, str(a), str(a), b)
        if e_i == 1:
            a = montgomery_multiplication(m, str(a), str(x_mark), b)
    a = montgomery_multiplication(m, str(a), '1', b)
    return a


if __name__ == '__main__':
    # p = generate_prime(1024)
    # test = exponentiation(2, 20000, p)
    # print(f'prime number = {p}\ntest = {test}')
    # test_2 = right_to_left_binary_exponentiation(2, 2048)
    # print(test_2)
    # test_3 = left_to_right_binary_exponentiation(2, 2048)
    # print(test_3)
    # test_4 = addition_chaining_exponentiation(7, 256, 13)
    # print(test_4)
    # test_5 = fast_exp(7, 90, 13)
    # print(test_5)
    # x_test = getrandbits(256)  # numero entero aleatorio de 256 bits
    # e_test = getrandbits(17)  # numero entero aleatorio de 17 bits
    # m_test = generate_prime(2048)  # numero primo aleatorio de 2048 bits
    x_test = '74106370990897111733726697610291693740084871859972769535694796516080724611830'
    e_test = '124319'
    m_test = '27413366002820230322072907355559122582516678742370207929685671695212700808303151287019156616423039212980145922229823610952432233625831458475095402409103473551393007531153828418020672534081127625755694875659693569902143855798574801379946802472887062304423746764962290910345192388589018924955811319748569908569424344288416765909599187285235154341512974945101833428000332672361167318805525666105548632965546426898629049846851807364287897619584023274148502253610067589211387825098204670049432541337058945013020546749849495939011534174762172031299633826967292511187902996718032704849546819371630507805655746930204038231093'
    # print(f'x = {x_test}')
    # print(f'e = {e_test}')
    # print(f'm = {m_test}')
    # test_6 = montgomery_exponentiation(x_test, e_test, m_test, 10)
    # print(test_6)
    # test_7 = right_to_left_binary_exponentiation(int(x_test), int(e_test), int(m_test))
    # print(test_7)
    # test_8 = left_to_right_binary_exponentiation(int(x_test), int(e_test), int(m_test))
    # print(test_8)
    # test_9 = fast_exp(int(x_test), int(e_test), int(m_test))  # una de las mas rapida
    # print(test_9)
    test_10 = addition_chaining_exponentiation(int(x_test), int(e_test), int(m_test))       # una de las mas rapida
    print(test_10)

