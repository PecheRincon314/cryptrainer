from math import floor

from prime_field_arithmetic.radix_b_representation import radix_b_representation


def change_to_radix(a: int, radix: int) -> int:
    q = 0
    a_sub_i = list(str(a)[::-1])
    for i in range(len(a_sub_i)):
        q += int(a_sub_i[i]) * (radix ** i)
    return q


def list_to_int(a: list) -> int:
    summation = 0
    a = a[::-1]
    for i in range(len(a)):
        summation += a[i] * (10 ** i)
    return summation


def barrett_modular_reduction(x: int, m: int, b: int, k: int) -> int:
    """

    :param x: positive integer in radix b representation
    :param m: positive integer in radix b representation
    :param b: radix b representation
    :param k: exponent of b such that the radix b is chosen to be close to the word-size of the processor.
    :return: x mod m given x and m

    requirements: x = (x2k−1 ··· x1x0)b , m = (mk−1 ··· m1m0)b
    """
    m = change_to_radix(m, b)
    miu = floor((b ** (2 * k)) / m)
    x = change_to_radix(x, b)
    q_sub_1 = floor(x / (b ** (k - 1)))
    q_sub_2 = q_sub_1 * miu
    q_sub_3 = floor(q_sub_2 / (b ** (k + 1)))
    r_sub_1 = x % (b ** (k + 1))
    r_sub_2 = (q_sub_3 * m) % (b ** (k + 1))
    r = r_sub_1 - r_sub_2
    while r >= m:
        r -= m
    return list_to_int(radix_b_representation(r, b))


if __name__ == '__main__':
    barret_test = barrett_modular_reduction(313221, 233, 4, 3)
    print(barret_test)
