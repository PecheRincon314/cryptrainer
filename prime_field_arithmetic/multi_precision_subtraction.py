def multi_precision_subtraction(x: str, y: str, radix: int, n_digits: int) -> int:
    """

    :param x: numero con n digitos (minuendo), x > y
    :param y: numero con n digitos (sustraendo)
    :param radix: base numerica para ralizar la operacion de resta
    :param n_digits: numero de digitos que tiene el minuendo y sustraendo, tienen que ser iguales.
    :return: Resto o diferencia
    """
    c = 0
    summation_z_i = [0] * n_digits
    for i in range(n_digits):
        z_sub_i = int(x[::-1][i]) - int(y[::-1][i]) + c
        summation_z_i[i] = z_sub_i % radix
        if z_sub_i < 0:
            c = -1
        else:
            c = 0
    z = 0
    for i in range(n_digits):
        z += summation_z_i[i] * (radix ** i)
    return z


if __name__ == '__main__':
    num_1 = '1010'
    num_2 = '0011'
    b = 2
    n = 4
    test = multi_precision_subtraction(num_1, num_2, b, n)
    print(f'subtraction: {num_1} - {num_2} = {test} en base {b}')
