from prime_field_arithmetic.generating_random_prime import generate_prime
from prime_field_arithmetic.multi_precision_subtraction import multi_precision_subtraction


def subtraction(x: int, y: int, prime: int) -> int:
    """

    :param x: integer number
    :param y: integer number
    :param prime: prime number
    :return: general modular subtraction
    """
    return ((x % prime) - (y % prime)) % prime


def subtraction_in_fp(a: int, b: int, prime: int, radix: int) -> int:
    """

    :param a: non-negative integer
    :param b: non-negative integer
    :param prime: prime number
    :param radix: the base of a system of numeration.
    :return: modular subtraction

    requirement: Let a and b be non-negative integers with a > b and a, b < p.
    """
    a = str(a)
    b = str(b)
    if len(a) > len(b):
        b = '0' * (len(a) - len(b)) + b
    elif len(a) < len(b):
        a = '0' * (len(b) - len(a)) + a
    c = multi_precision_subtraction(a, b, radix, len(a))
    if c < 0:
        c = c + prime
    return c


if __name__ == '__main__':
    p = generate_prime(8)
    test = subtraction_in_fp(39, 23, p, 10)
    print(f'test = {test} prime = {p}')
    test2 = subtraction(-39, -23, p)
    print(f'test2 = {test2} prime = {p}')
