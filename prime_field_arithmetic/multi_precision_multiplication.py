def multi_precision_multiplication(x: int, y: int, radix: int) -> int:
    """

    :param x: Multiplicando
    :param y: Multiplicador
    :param radix: Base numerica
    :return: Producto de dos numeros enteros positivos

    reference : Menezes, A.J., van Oorschot, P.C., Vanstone, S.A.: Handbook of Applied Cryptography. The
                CRC Press Series on Discrete Mathematics and Its Applications. CRC, Florida, USA (1997)
    """
    x = list(str(x)[::-1])
    y = list(str(y)[::-1])
    n = len(x)
    t = len(y)
    w_i = [0] * (n + t)
    # print(f'x = {x}, y = {y}, n = {n}, t = {t}')
    # print(f'w_i = {w_i}')
    for i in range(t):  # i = 0 to t-1
        c = 0
        for j in range(n):  # j = 0 to n-1
            uv_b = str(w_i[i + j] + (int(x[j]) * int(y[i])) + c)
            if len(uv_b) == 1:
                w_i[i + j] = int(uv_b)
                c = 0
            else:
                w_i[i + j] = int(uv_b[1])
                c = int(uv_b[0])
        w_i[i + n] = c
    w = 0
    for i in range(len(w_i)):
        w += w_i[i] * (radix ** i)
    return w


if __name__ == '__main__':
    # test = multi_precision_multiplication(2 ** 2048, 2 ** 2048, 10)
    test = multi_precision_multiplication(9274, 847, 10)
    # test = multi_precision_multiplication(101111000110000011001000, 1111011, 2)
    print(f'test = {test}')
