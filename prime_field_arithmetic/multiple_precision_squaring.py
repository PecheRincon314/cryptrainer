def multiple_precision_squaring(x: int, radix: int) -> int:
    """

    :param x: Base de la potenciacion
    :param radix: Base numérica
    :return: Potencia de x al cuadrado
    """
    x = list(str(x)[::-1])
    t = len(x)
    w_i = [0] * (2 * t)
    for i in range(t):
        uv_b = str(w_i[2 * i] + (int(x[i]) * int(x[i])))
        if len(uv_b) == 1:
            w_i[2 * i] = int(uv_b)
            c = 0
        elif len(uv_b) == 2:
            w_i[2 * i] = int(uv_b[1])
            c = int(uv_b[0])
        else:
            w_i[2 * i] = int(uv_b[2])
            c = int(uv_b[0] + uv_b[1])
        for j in range(i + 1, t):
            uv_b = str(w_i[i + j] + (2 * (int(x[j]) * int(x[i]))) + c)
            if len(uv_b) == 1:
                w_i[i + j] = int(uv_b)
                c = 0
            elif len(uv_b) == 2:
                w_i[i + j] = int(uv_b[1])
                c = int(uv_b[0])
            else:
                w_i[i + j] = int(uv_b[2])
                c = int(uv_b[0] + uv_b[1])
        w_i[i + t] = c
    w = 0
    for i in range(len(w_i)):
        w += w_i[i] * (radix ** i)

    return w


if __name__ == '__main__':
    print("Hello world!")
    test = multiple_precision_squaring(733632768603562266898029184610413092619436993429899225, 10)
    test_python = 733632768603562266898029184610413092619436993429899225 ** 2
    print(f'test =        {test}')
    print(f'test_python = {test_python}')
