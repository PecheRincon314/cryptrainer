def multi_precision_addition(x: str, y: str, radix: int, n_digits: int) -> int:
    """Funcion que suma dos numeros en una base numerica determinada.
       Condiciones:
            - se debe indicar el numero de digitos (n_digits) y la base (radix) a utilizar en la operacion
            - ambos numeros deben tener la misma cantidad de digitos
            - el valor de la base tiene que ser mayor o igual que 2
            - los digitos de los numeros deben ser mayores o iguales que cero y menores que la base
    """
    c = 0
    integer_z = [0] * (n_digits + 1)
    for i in range(n_digits):
        z_sub_i = int(x[::-1][i]) + int(y[::-1][i]) + c
        integer_z[i] = z_sub_i % radix
        if z_sub_i >= radix:
            c = 1
        else:
            c = 0
        integer_z[n_digits] = c
    z = 0
    for i in range(n_digits + 1):
        z += integer_z[i] * (radix ** i)
    return z


if __name__ == '__main__':
    a = '01110'
    b = '01010'
    base = 2
    numero_digitos = 5
    add = multi_precision_addition(a, b, base, numero_digitos)
    print(f'operacion: {a} + {b} = {add} en base {base}')
