from math import floor


def radix_b_representation(a: int, b: int) -> list:
    """

    :param a: integer >= 0
    :param b: radix b representation >= 2
    :return: the base b representation a = (an ··· a1a0)_b, where n ≥ 0 and an != 0 if n ≥ 1.
    """
    a_sub_i = list()
    x = a
    q = x // b
    a_sub_i.append(x - q * b)
    while q > 0:
        x = q
        q = x // b
        a_sub_i.append(x - q * b)

    return a_sub_i[::-1]


if __name__ == '__main__':
    # test = radix_b_representation(222, 4)
    test = radix_b_representation(3888471767846831421601062579370391508854596531818755807580144861042221772660, 10)
    print(test)
    test_2 = radix_b_representation(91, 2)
    print(test_2)
