from sys import getsizeof

from Crypto.Util import number
from Crypto import Random

"""getsizeof: Devuelve el tamaño de un objeto en bytes. Solo se tiene en cuenta el consumo de memoria atribuido 
   directamente al objeto, no el consumo de memoria de los objetos a los que se refiere."""


def generate_prime(nbits: int) -> int:
    return number.getPrime(nbits, randfunc=Random.get_random_bytes)


if __name__ == '__main__':
    p = generate_prime(1024)
    print(f'prime number = {p}\nmemory size: {getsizeof(p)} bytes')
