from math import ceil, log
from typing import Union


def sgn(x: int) -> int:
    """Función signo"""
    if x >= 0:
        return 1
    return -1


def euclid_extended_algorithm(a: int, b: int) -> list:
    """

    :param a: integer number
    :param b: integer number
    :return: mcd(a, b), t y s

    Función que implementa el Algoritmo Extendido de Euclides.
    Retorna una lista con: [mcd(a,b), s, t]
    Algoritmo 2.3: Algoritmo Extendido de Euclides - Walter Mora Flores pag. 34

    """
    c = abs(a)
    d = abs(b)
    c_1 = 1
    c_2 = 0
    d_1 = 0
    d_2 = 1
    while d != 0:
        q = c // d
        r = c - q * d
        r_1 = c_1 - q * d_1
        r_2 = c_2 - q * d_2
        c = d
        c_1 = d_1
        c_2 = d_2
        d = r
        d_1 = r_1
        d_2 = r_2
    return [abs(c), c_1 * sgn(a) * sgn(c), c_2]


def inverse_euclides(a: int, m: int) -> Union[None, int]:
    """

    :param a: integer a ∈ Z_m
    :param m: positive integer m > 1
    :return: a^(−1) if mcd(a,m) = 1

    Algoritmo 8.4: Inverso Multiplicativo mod m. Wlater Mora pag. 170
    """
    mcd = euclid_extended_algorithm(a, m)
    if mcd[0] > 1:
        return None
    return mcd[1] % m


def binary_euclidean_algorithm(a: int, p: int) -> int:
    """

    :param a: positive integer a ∈ Fp.
    :param p: prime number
    :return: a^(-1) mod p

    Algorithm 16. BEA for Inversion in Fp.
    Article: Efficient Software-Implementation of Finite Fields with Applications to Cryptography.pdf pag. 24

    """
    u = a
    v = p
    b = 1
    c = 0
    while u != 1 and v != 1:
        while u % 2 == 0:
            u = u // 2
            if b % 2 == 0:
                b = b // 2
            else:
                b = (b + p) // 2
        while v % 2 == 0:
            v = v // 2
            if c % 2 == 0:
                c = c // 2
            else:
                c = (c + p) // 2
        if u >= v:
            u = u - v
            b = b - c
        else:
            v = v - u
            c = c - b
    if u == 1:
        return b % p
    return c % p


def montgomery_inversion(a: int, p: int) -> int:
    """

    :param a: positive number where a ∈ [1, p − 1]
    :param p: prime number
    :return:  MonInv(a) where r = a^−1 * 2^n (mod p)

    Article: The Montgomery Modular Inverse - Revisited E. Sava¸s, C¸. K. Ko¸c
    """
    u = p
    v = a
    r = 0
    s = 1
    k = 0
    n = ceil(log(p, 2))
    while v > 0:
        if u % 2 == 0:
            u = u // 2
            s = 2 * s
        elif v % 2 == 0:
            v = v // 2
            r = 2 * r
        elif u > v:
            u = (u - v) // 2
            r = r + s
            s = 2 * s
        elif v >= u:
            v = (v - u) // 2
            s = r + s
            r = 2 * r
        k = k + 1
    if r >= p:
        r = r - p
    for i in range(1, k - n + 1):
        if r % 2 == 0:
            r = r // 2
        else:
            r = (r + p) // 2
    return r


if __name__ == '__main__':
    # test = euclid_extended_algorithm(23 ** 30, 56 ** 34)
    # print(f'test mcd = {test}')
    # print(f'mcd = {(test[1] * (23 ** 30)) + (test[2] * (56 ** 34))}')
    # test_inverse = inverse_euclides(2 ** 50000, 135700322192569531820776140714962017607510852702148884216379163933531468829582643241755783065331876687132481995010273647881582093735686746102753803150390474853429592617310746451646680932692571627230148416650427374266665596734095831273191943250969103515620550191507784582894141937698027789196013097027714199847)
    # print(f'test inverse = {test_inverse}')
    # test_2 = binary_euclidean_algorithm(2 ** 50000, 135700322192569531820776140714962017607510852702148884216379163933531468829582643241755783065331876687132481995010273647881582093735686746102753803150390474853429592617310746451646680932692571627230148416650427374266665596734095831273191943250969103515620550191507784582894141937698027789196013097027714199847)
    # print(test_2)
    test_3 = montgomery_inversion(8, 129)
    print(test_3)
