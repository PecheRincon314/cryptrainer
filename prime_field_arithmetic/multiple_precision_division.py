from typing import Tuple
from math import floor


def multiple_precision_division(x: int, y: int, radix: int) -> Tuple[int, int]:
    """

    :param x: Dividend positive integer number
    :param y: Divisor positive integer number
    :param radix: The base of a system of numeration.
    :return: Quotient and remainder of the division of two positive integers

    conditions: n >= t >= 1, y_t != 0
    """
    x_i = list(str(x)[::-1])
    y_t = list(str(y)[::-1])
    n = len(x_i) - 1
    t = len(y_t) - 1
    q = [0] * (n - t + 1)
    while x >= (y * (radix ** (n - t))):
        q[n - t] += 1
        x -= y * (radix ** (n - t))

    for i in range(n, t, -1):

        if x_i[i] == y_t[t]:
            q[i - t - 1] = radix - 1

        else:
            q[i - t - 1] = floor(((int(x_i[i]) * radix) + int(x_i[i - 1])) / int(y_t[t]))

        while (q[i - t - 1] * ((int(y_t[t]) * radix) + int(y_t[t - 1]))) > (
                (int(x_i[i]) * (radix ** 2)) + (int(x_i[i - 1]) * radix) + int(x_i[i - 2])):
            q[i - t - 1] -= 1

        x -= q[i - t - 1] * y * (radix ** (i - t - 1))

        if x < 0:
            x += y * (radix ** (i - t - 1))
            q[i - t - 1] -= 1

        x_i = list(str(x)[::-1])
        if len(x_i) < i:
            x_i = x_i + ['0'] * (i - len(x_i))

    remainder = 0
    for i in range(len(x_i)):
        remainder += int(x_i[i]) * (radix ** i)

    quotient = 0
    for i in range(len(q)):
        quotient += q[i] * (radix ** i)

    return quotient, remainder


if __name__ == '__main__':
    print("Hello world!")
    test = multiple_precision_division(721948327, 84461, 10)
    # test = multiple_precision_division(2 ** 2048, 84461, 10)
    print(f'test = {test}')
