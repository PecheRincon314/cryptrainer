.. Cryptrainer documentation master file, created by
   sphinx-quickstart on Tue Oct  5 23:07:19 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentación de Cryptrainer
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   prime_field.rst
   binary_fields.rst


Indices y tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
